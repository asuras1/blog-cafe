package model

import (
	"Backend-PRJ/dataStore/postgres"
)

type User struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (u *User) CreateUser(a []byte) error {
	const queryCreateUser = "INSERT INTO blogger (username, email, password) VALUES($1,$2,$3);"
	_, err := postgres.Db.Exec(queryCreateUser, u.Username, u.Email, a)
	return err
}

func (u *User) Check() ([]byte, error) {
	var dbHash []byte
	const queryCheck = "SELECT * FROM blogger WHERE username = $1;"
	err := postgres.Db.QueryRow(queryCheck, u.Username).Scan(&u.Username, &u.Email, &dbHash)
	return dbHash, err
}
