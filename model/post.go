package model

import (
	"Backend-PRJ/dataStore/postgres"
	"fmt"
)

type Post struct {
	Post_id      int    `json:"post_id"`
	Post_title   string `json:"post_title"`
	Post_content string `json:"post_content"`
	Post_img     string `json:"post_img"`
	Username     string `json:"username"`
}

func (p *Post) Create() error {
	const creatPost = "INSERT INTO post (post_title, post_content, post_img, username) VALUES($1, $2, $3, $4);"
	_, err := postgres.Db.Exec(creatPost, p.Post_title, p.Post_content, p.Post_img, p.Username)
	return err
}

func (p *Post) GetPost() error {
	const getpost = "SELECT * FROM post WHERE post_id = $1;"
	err := postgres.Db.QueryRow(getpost, p.Post_id).Scan(&p.Post_id, &p.Post_title, &p.Post_content, &p.Post_img, &p.Username)
	return err
}

func (p *Post) DeletePost() error {
	const deletepost = "DELETE  FROM post WHERE post_id = $1 RETURNING post_id;"
	return postgres.Db.QueryRow(deletepost, p.Post_id).Scan(&p.Post_id)
}

func (p *Post) UpdatePost(pid int) error {
	const updatepost = "UPDATE post set post_title = $1, post_content = $2, post_img = $3  WHERE post_id = $4 RETURNING post_id;"
	fmt.Println(p.Post_title, p.Post_content, p.Post_img, pid, "here")
	err := postgres.Db.QueryRow(updatepost, p.Post_title, p.Post_content, p.Post_img, pid).Scan(&p.Post_id)
	fmt.Println(err)
	return err
}

func GetAllPost() ([]Post, error) {
	const getall = "SELECT * FROM post;"
	table, err := postgres.Db.Query(getall)
	if err != nil {
		return nil, err
	}
	posts := []Post{}

	for table.Next() {
		var p Post
		err := table.Scan(&p.Post_id, &p.Post_title, &p.Post_content, &p.Post_img, &p.Username)
		if err != nil {
			return nil, err
		}
		posts = append(posts, p)
	}
	table.Close()
	return posts, nil
}

func GetAllSpecific(email string) ([]Post, error) {
	const getPostsByEmailQuery = "SELECT * FROM post WHERE username = $1;"
	table, err := postgres.Db.Query(getPostsByEmailQuery, email)
	if err != nil {
		return nil, err
	}
	defer table.Close()

	posts := []Post{}
	for table.Next() {
		var p Post
		err := table.Scan(&p.Post_id, &p.Post_title, &p.Post_content, &p.Post_img, &p.Username)
		if err != nil {
			return nil, err
		}
		posts = append(posts, p)
	}

	return posts, nil
}
