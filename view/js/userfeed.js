const username = localStorage.getItem("username")
console.log(username)

window.onload = function() {
    fetch('/posts/'+username) 
    .then(response => response.text())
    .then(data => showallposts(data))
    .catch(e=>{
        alert(e)
    })
}

function showallposts(a) {
    const posts = JSON.parse(a)
    // console.log(posts)
    posts.forEach(stud => {
        // console.log(stud, "pooo")

      newPost(stud)
})
}
 
function newPost(post){
    let c = document.querySelector(".content");
  
    let p = document.createElement("div")
    p.classList.add("post")
    
    let u = document.createElement("div")
    u.classList.add("user-")
    let ph = document.createElement("h3")
    ph.innerHTML=post.username

    //Dropdown --------------------------------------
    let dropdown = document.createElement("div");
    dropdown.classList.add("dropdown");

    let button = document.createElement("button");
    button.setAttribute("class", "dropdown-button");
    button.innerHTML = "&#10247;"
    dropdown.appendChild(button);

    let dropdown_content = document.createElement("div");
    dropdown_content.setAttribute("class", "dropdown-content");

    let button1 = document.createElement("button");
    button1.innerHTML = "Edit";
    button1.addEventListener("click",function(){updates(post.post_id)})
    dropdown_content.appendChild(button1);

    let button2 = document.createElement("button");
    button2.innerHTML = "Delete";
    dropdown_content.appendChild(button2);
    button2.addEventListener("click",function(){deletes(post.post_id)})

    dropdown.appendChild(dropdown_content);
    
    u.appendChild(ph)
    p.appendChild(u)
    u.appendChild(dropdown)

    let tit = document.createElement("p")
    tit.classList.add("title")
    tit.innerHTML = post.post_title
    p.appendChild(tit)

    let imfs = document.createElement("img")
    imfs.classList.add("post_img")
    imfs.src = post.post_img
    p.appendChild(imfs)

    let main = document.createElement("p")
    main.classList.add("main")
    main.innerHTML = post.post_content
    p.appendChild(main)

    c.appendChild(p)
}

// Add event listeners to the edit and delete options
document.querySelector(".edit-option").addEventListener("click", function() {
    // Code to handle the edit option click
    console.log("Edit option clicked");
  });
  
  document.querySelector(".delete-option").addEventListener("click", function() {
    // Code to handle the delete option click
    console.log("Delete option clicked");
  });
  
  function deletes(id){
    if (confirm("Are you sure to delete this post?")){
        fetch("/post/"+id,{
            method:"DELETE",
            headers:{"content-type":"application/json; charset=utf-8"}
        })
        .then(resp =>{
            if(resp.ok){
                window.location.href = "userfeed.html"
            }else{
                throw new Error (resp.statusText)
            }
        })
        .catch(e =>{
            alert(e)
        })
    }
  }

  function updates (a){
    localStorage.setItem("id",a)
    window.location.href = "update.html"
  }