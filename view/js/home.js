window.onload = function() {
    fetch('/posts')
    .then(response => response.text())
    .then(data => showallposts(data))
}

function showallposts(a) {
    const posts = JSON.parse(a)
    // console.log(posts)
    posts.forEach(stud => {
        // console.log(stud, "pooo")

      newPost(stud)
})
}

function newPost(post){
    console.log(post)
    let c = document.querySelector(".content");
  
    let p = document.createElement("div")
    p.classList.add("post")

    let u = document.createElement("div")
    u.classList.add("user-")
    let ph = document.createElement("h3")
    ph.innerHTML=post.username
    let pp = document.createElement("p")
    pp.innerHTML = " "
    u.appendChild(ph)
    u.appendChild(pp)
    p.appendChild(u)

    let tit = document.createElement("p")
    tit.classList.add("title")
    tit.innerHTML = post.post_title
    p.appendChild(tit)

    let imfs = document.createElement("img")
    imfs.classList.add("post_img")
    imfs.src = post.post_img
    p.appendChild(imfs)

    let main = document.createElement("p")
    main.classList.add("main")
    main.innerHTML = post.post_content
    p.appendChild(main)

    c.appendChild(p)
}

function logout() {
    fetch("/logout")
    .then(res => {
        if (res.ok){
            window.open("Login.html","_self")
        }else{
            throw new Error(res.statusText)
        }
    }).catch(e =>{
        alert(e)
    })
}