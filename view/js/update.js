var id = localStorage.getItem("id")
window.onload = function(){
    fetch("/post/"+id)
    .then(resp => resp.text())
    .then(data => showDatas(data))
    .catch(e => {
        alert(e)
    })
}
document.querySelector("#picture").addEventListener("change", function(){
   
    const dataurl = new FileReader();
    dataurl.addEventListener("load", ()=>{
        localStorage.setItem("image-dataurl", dataurl.result)
    })
    dataurl.readAsDataURL(this.files[0])
})
function update(){
    var data ={
        post_title : document.getElementById("title").value,
        post_content : document.getElementById("area").value,
        post_img : localStorage.getItem("image-dataurl")
    }
    fetch("/post/"+id,{
        method:"PATCH",
        body:JSON.stringify(data),
        headers:{"content-type":"application/json; charset=utf-8"}
    }).then(resp =>{
        if (resp.ok){
            window.location.href = "userfeed.html"
        }else{
            throw new Error(resp.statusText)
        }
    })
    .catch(e =>{
        alert(e)
    })
}

function showDatas(data){
    const post = JSON.parse(data)
    newPosts(post)
}

function newPosts(ele){
    document.getElementById("title").value = ele.post_title;
    document.getElementById("area").value = ele.post_content;
}