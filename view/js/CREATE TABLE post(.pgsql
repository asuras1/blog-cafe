CREATE TABLE post(
    post_id serial PRIMARY KEY,
    post_title VARCHAR(50) NOT NULL,
    post_content TEXT,
    post_img TEXT,
    username VARCHAR(45) not null,
    constraint username foreign key (username) references blogger
    (username) on delete cascade on update cascade
);

CREATE TABLE blogger (
     username varchar primary key,
     email varchar not null,
     password text
);
