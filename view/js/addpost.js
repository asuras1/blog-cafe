var username = localStorage.getItem("username")
// alert(email)

document.querySelector("#picture").addEventListener("change", function(){
   
    const dataurl = new FileReader();
    dataurl.addEventListener("load", ()=>{
        localStorage.setItem("image-dataurl", dataurl.result)
    })
    dataurl.readAsDataURL(this.files[0])
})
function upload() {
    var getData = {
	    post_title: document.querySelector(".title-field").value,
	    post_content: document.querySelector("#area").value,
	    post_img: localStorage.getItem("image-dataurl"),
	    username: username
    }
    console.log(getData)

    fetch("/post", {
        method: "POST",
        body: JSON.stringify(getData),
        headers: {"Content-type":"application/json; charset=UTF-8"}
    })
    .then(resp =>{
        if (resp.status == 202) {
            window.open("addpost.html","_self") 
        }else if (resp.status == 401){
            alert("Invalid login, try again")
        }else{
            alert("Post uploaded")
            window.open("addpost.html","_self") 
        }
    }).catch(e => {
        alert(e,"jjjjj")
    })

}