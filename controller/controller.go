package controller

import (
	"Backend-PRJ/model"
	"Backend-PRJ/utils/httpResp"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/lib/pq"
	"golang.org/x/crypto/bcrypt"
)

func AddPost(w http.ResponseWriter, r *http.Request) {
	//cookie verification
	if !VerifyCookie(w, r) {
		return
	}
	var p model.Post
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&p); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}

	defer r.Body.Close()
	saveErr := p.Create()
	if saveErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
		return
	}

	httpResp.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Post added"})
}

// Get POST
func GetPost(w http.ResponseWriter, r *http.Request) {
	//cookie verification
	if !VerifyCookie(w, r) {
		return
	}
	pid := mux.Vars(r)["id"]
	fmt.Println("helllo", pid)
	pidint := getIntPostID(pid)
	p := model.Post{Post_id: pidint}

	err := p.GetPost()

	if err != nil {
		switch err {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Post not found")
			fmt.Println("Post not found", err)
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, err.Error())
			fmt.Println("sdfskdf", err)
		}
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, p)
	fmt.Println("success", p)

}

// Get specific Post
func GetSpecificHandler(w http.ResponseWriter, r *http.Request) {
	//cookie verification
	if !VerifyCookie(w, r) {
		return
	}
	email := mux.Vars(r)["username"]
	// ps := model.Post{Email: email}

	posts, err := model.GetAllSpecific(email)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, posts)
}

// convert string stdID to int
func getIntPostID(userIdParam string) int {
	userId, _ := strconv.ParseInt(userIdParam, 10, 0)
	return int(userId)
}

// Delete Post
func DeletePost(w http.ResponseWriter, r *http.Request) {
	//cookie verification
	if !VerifyCookie(w, r) {
		return
	}
	pid := mux.Vars(r)["id"]
	pidint := getIntPostID(pid)
	p := model.Post{Post_id: pidint}

	err := p.DeletePost()

	if err != nil {
		switch err {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Post not found")
			fmt.Println("Post not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, "deleted the post")
}

// Update Post
func UpdatePost(w http.ResponseWriter, r *http.Request) {
	//cookie verification
	if !VerifyCookie(w, r) {
		return
	}
	pid := mux.Vars(r)["id"]
	fmt.Println("helllooo", pid)
	pidint := getIntPostID(pid)

	var p model.Post
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&p); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		fmt.Println("1", err)
		return
	}

	err := p.UpdatePost(pidint)

	if err != nil {
		switch err {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Post not found")
			fmt.Println("Post not found", err)
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, err.Error())
			fmt.Println("2", err)
		}
		return
	}
	fmt.Println("last", p)
	httpResp.RespondWithJSON(w, http.StatusCreated, p)
}

// Get all Post
func GetAllPost(w http.ResponseWriter, r *http.Request) {
	posts, err := model.GetAllPost()
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, posts)
}

// Sign Up
func SignupHandler(w http.ResponseWriter, r *http.Request) {
	var user model.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "couldn't decode the request")
		fmt.Println("error in decoding the request", err)
		return
	}
	var hash []byte
	hash, err = bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		fmt.Println("bcrypt err:", err)
		return
	}
	fmt.Println("hash:", hash)
	fmt.Println("string(hash)", string(hash))
	addErr := user.CreateUser(hash)
	if addErr != nil {
		if e, ok := addErr.(*pq.Error); ok {
			if e.Code == "23505" {
				fmt.Print("duplicate key error")
				httpResp.RespondWithError(w, http.StatusNotAcceptable, "duplicate key error")
				return
			}
		} else {
			fmt.Println("error in inserting the data")
			httpResp.RespondWithError(w, http.StatusBadRequest, "error in inserting the data")
			return
		}
	}

	fmt.Println("successful")
	httpResp.RespondWithJSON(w, http.StatusCreated, "added successfully")
}

// Login
func LoginHandler(w http.ResponseWriter, r *http.Request) {
	var user model.User
	var err = json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		fmt.Println("error in decoding the request")
		return
	}
	dbHash, checkErr := user.Check()
	if checkErr != nil {
		switch checkErr {
		case sql.ErrNoRows:
			fmt.Print("invalid login")
			httpResp.RespondWithError(w, http.StatusNotFound, "Invalid login, try again")

		default:
			fmt.Print("error in getting the data to compare", checkErr)
			httpResp.RespondWithError(w, http.StatusBadRequest, "error with the database")

		}
		return
	}
	pwdErr := bcrypt.CompareHashAndPassword([]byte(dbHash), []byte(user.Password))
	if pwdErr != nil {
		fmt.Println("invalid with password")
		httpResp.RespondWithError(w, http.StatusNotFound, "Invalid login, try again")
		return
	}
	//create a cookie
	cookie := http.Cookie{
		Name: "admin-cookie",
		// Value: email +admin.Password,
		Value:   "Tshering",
		Expires: time.Now().Add(30 * time.Minute),
		Secure:  true,
	}
	//set cookie and send back to client
	http.SetCookie(w, &cookie)
	fmt.Println("login successful")
	httpResp.RespondWithJSON(w, http.StatusAccepted, map[string]string{"message": "successful"})
}
func LogoutHandler(w http.ResponseWriter, r *http.Request) {
	http.SetCookie(w, &http.Cookie{
		Name:    "admin-cookie",
		Expires: time.Now(),
	})
	fmt.Println("logout successful")
	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"message": "logout successful"})
}

func VerifyCookie(w http.ResponseWriter, r *http.Request) bool {
	cookie, err := r.Cookie("admin-cookie")
	if err != nil {
		switch err {
		case http.ErrNoCookie:
			httpResp.RespondWithError(w, http.StatusSeeOther, "cookie not set")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, "internal server error")
		}
		return false
	}
	if cookie.Value != "Tshering" {
		httpResp.RespondWithError(w, http.StatusSeeOther, "invalid cookie")
		return false
	}
	return true
}
