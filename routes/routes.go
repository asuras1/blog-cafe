package routes

import (
	"Backend-PRJ/controller"
	"net/http"

	"github.com/gorilla/mux"
)

func InitializeRoutes() {
	router := mux.NewRouter()

	router.HandleFunc("/signup", controller.SignupHandler).Methods("POST")

	router.HandleFunc("/login", controller.LoginHandler).Methods("POST")

	router.HandleFunc("/logout", controller.LogoutHandler).Methods(("GET"))

	router.HandleFunc("/post", controller.AddPost).Methods("POST")
	router.HandleFunc("/post/{id}", controller.GetPost).Methods("GET")
	router.HandleFunc("/post/{id}", controller.DeletePost).Methods("DELETE")
	router.HandleFunc("/post/{id}", controller.UpdatePost).Methods("PATCH")
	router.HandleFunc("/posts", controller.GetAllPost).Methods("GET")
	router.HandleFunc("/posts/{username}", controller.GetSpecificHandler).Methods("GET")

	fhandler := http.FileServer(http.Dir("./view"))

	router.PathPrefix("/").Handler(fhandler)

	err := http.ListenAndServe(":4340", router)
	if err != nil {
		return
	}

}
